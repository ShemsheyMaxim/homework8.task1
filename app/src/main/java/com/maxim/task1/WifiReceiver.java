package com.maxim.task1;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

import java.util.Objects;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

/**
 * Created by Максим on 27.06.2017.
 */

public class WifiReceiver extends BroadcastReceiver {

    public static final int WIFI_NOTIFICATION_ID = 2;

    @Override
    public void onReceive(Context context, Intent intent) {
        String state = NotificationUtils.isWifiModeOn(intent) ? "wifi enable" : "wife disable";
        Log.d("wifi mode", "state");

//        create intent (click notification)
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(notificationIntent);

        if (Objects.equals(state, "wifi disable")) {
            NotificationUtils.sendWifiModeNotification(context);
//            MainActivity.statusWifi.setText(MainActivity.statusOff);
        } else {
            NotificationUtils.cancelWifiModeNotification(context);
//            MainActivity.statusWifi.setText(MainActivity.statusOn);
        }
    }
}