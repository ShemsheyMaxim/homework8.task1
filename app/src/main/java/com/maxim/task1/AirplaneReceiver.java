package com.maxim.task1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Objects;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static com.maxim.task1.R.drawable.airplane_mode_off;

/**
 * Created by Максим on 25.06.2017.
 */

public class AirplaneReceiver extends BroadcastReceiver {

    public static final int AIRPLANE_NOTIFICATION_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {

        String state = NotificationUtils.isAirplaneModeOn(context) ? "AirplaneMode enable" : "AirplaneMode disable";
        Log.d("airplane mode", "state");

//        create intent (click notification)
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(notificationIntent);

        if (Objects.equals(state, "AirplaneMode disable")) {
            NotificationUtils.sendAirplaneModeNotification(context);
//            MainActivity.statusAirplane.setText(MainActivity.statusOff);
        } else {
            NotificationUtils.cancelAirplaneModeNotification(context);
//            MainActivity.statusAirplane.setText(MainActivity.statusOn);
        }
    }
}


