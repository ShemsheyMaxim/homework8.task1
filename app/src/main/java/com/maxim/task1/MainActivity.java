package com.maxim.task1;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    public TextView airplaneMode;
    public TextView wifiMode;
    public TextView statusAirplane;
    public TextView statusWifi;
    public final String statusOn = "On";
    public final String statusOff = "Off";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        airplaneMode = (TextView) findViewById(R.id.textView_airplane);
        statusAirplane = (TextView) findViewById(R.id.textView_status_airplane);
        wifiMode = (TextView) findViewById(R.id.textView_wifi);
        statusWifi = (TextView) findViewById(R.id.textView_status_wifi);

        if (!NotificationUtils.isAirplaneModeOn(this)) {
            NotificationUtils.sendAirplaneModeNotification(this);
            statusAirplane.setText(statusOff);
        } else {
            NotificationUtils.cancelAirplaneModeNotification(this);
            statusAirplane.setText(statusOn);
        }

        if (!NotificationUtils.isWifiModeOn(this)) {
            NotificationUtils.sendWifiModeNotification(this);
            statusWifi.setText(statusOff);
        } else {
            NotificationUtils.cancelWifiModeNotification(this);
            statusWifi.setText(statusOn);
        }
    }

    @Override
    protected void onDestroy() {
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
        super.onDestroy();

    }
}
