package com.maxim.task1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import static com.maxim.task1.R.drawable.airplane_mode_off;
import static com.maxim.task1.R.drawable.samolet;
import static com.maxim.task1.R.drawable.wifi_off;

/**
 * Created by Максим on 26.06.2017.
 */

public class NotificationUtils {

    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    public static void sendAirplaneModeNotification(Context context) {
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(airplane_mode_off)
                        .setContentTitle("Airplane notification")
                        .setContentText("Большой брат следит за тобой!")
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.airplane_mode_off));
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(AirplaneReceiver.AIRPLANE_NOTIFICATION_ID, mBuilder.build());

    }

    public static void cancelAirplaneModeNotification(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(AirplaneReceiver.AIRPLANE_NOTIFICATION_ID);
    }

    public static boolean isWifiModeOn(Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        return networkInfo != null && networkInfo.isConnected();
    }

    public static boolean isWifiModeOn(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static void sendWifiModeNotification(Context context) {
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(wifi_off)
                        .setContentTitle("Wifi notification")
                        .setContentText("Большой брат подглядывает за тобой")
                        .setLights(Color.RED,0,1);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(WifiReceiver.WIFI_NOTIFICATION_ID, mBuilder.build());

    }

    public static void cancelWifiModeNotification(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(WifiReceiver.WIFI_NOTIFICATION_ID);
    }
}
